from Dictionaries_HMG import *
from Sets_HMG import *
from tkinter import *
import random


def run():
    counter = 0
    ev_total = 508
    t1.delete('1.0', END)
    pokemon_set.clear()
    while counter < 1:
        cc_evo_line = random.randint(1, 1)
        cc_evo = random.randint(1, 3)
        cc_item = random.randint(1, 13)
        cc_ability = random.randint(1, 3)
        cc_nature = random.randint(1, 25)
        cc_gender = random.randint(1, 2)
        cc_shiny = random.randint(1, 2)
        random.shuffle(values)
        random.shuffle(stats)
        cc_level = random.randint(1, 100)
        cc_happiness = random.randint(0, 255)
        stat_1 = ev_total - random.randint(0, ev_total)
        stat_2 = stat_1 - random.randint(0, stat_1)
        stat_3 = stat_2 - random.randint(0, stat_2)
        stat_4 = stat_3 - random.randint(0, stat_3)
        stat_5 = stat_4 - random.randint(0, stat_4)
        stat_6 = stat_5 - random.randint(0, stat_5)
        if cc_evo not in pokemon[cc_evo_line] and cc_evo_line in pokemon_set:
            pass
        if cc_evo in pokemon[cc_evo_line] and cc_evo_line not in pokemon_set and cc_ability in abilities[cc_evo_line][cc_evo]:
            t1.insert(END, str(pokemon[cc_evo_line][cc_evo]) + " (" + str(genders[cc_gender]) + ")" + " @ " + str(items[cc_item]) +
                      "\nAbility: " + str(abilities[cc_evo_line][cc_evo][cc_ability]) +
                      "\nLevel: " + str(cc_level) +
                      "\nShiny: " + str(shinies[cc_shiny]) +
                      "\nHappiness: " + str(cc_happiness) +
                      "\nEVs: " + str(stat_1) + " " + str(stats[0]) + " / " + str(stat_2) + " " + str(stats[1]) + " / " + str(stat_3) + " " + str(stats[2]) + " / " + str(stat_4) + " " + str(stats[3]) + " / " + str(stat_5) + " " + str(stats[4]) + " / " + str(stat_6) + " " + str(stats[5]) +
                      "\n" + str(natures[cc_nature]) + " Nature" +
                      "\n- " + str(values[0]) +
                      "\n- " + str(values[1]) +
                      "\n- " + str(values[2]) +
                      "\n- " + str(values[3]))
            pokemon_set.add(cc_evo_line)
            counter += 1


win = Tk()
win.title("Pokemon Generator")
f1 = Frame(win)
f1.grid(row=0, column=0)
f2 = Frame(win)
f2.grid(row=1, column=0)

gen_button = Button(f1, text="Generate", command=run, width=10, height=2)
gen_button.grid(row=0, column=0)

save_button = Button(f1, text="Save", width=10, height=2)
save_button.grid(row=0, column=1)

exit_button = Button(f1, text="Exit", width=10, height=2, command=exit)
exit_button.grid(row=0, column=2)

t1 = Text(f2, width=61, height=53)
t1.grid(row=0, column=0)

win.mainloop()
