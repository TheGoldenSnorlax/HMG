from tkinter import *
from HMG_Core_Logic import *

win = Tk()
f1 = Frame(win)
f1.grid(row=0, column=0)
f2 = Frame(win)
f2.grid(row=0, column=1)

b1 = Button(f1, text="Generate", command=gen)
b1.grid(row=0, column=0)

t1 = Text(f2)
t1.grid(row=0, column=0)

win.mainloop()
